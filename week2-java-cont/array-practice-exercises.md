### Exercises:
- create an array of 3 strings
- print out the second value in the array
- print out the eighth character of the first string
    - what happens if your first string has less than eight characters? 
    - how would you avoid that issue?
- add a fourth string to the array
    - can this be done directly? 
    - how can we accomplish this?
- define a method that can take any number of string variables and prints each provided string on a new line (using var args)