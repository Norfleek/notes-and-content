## TDD Exercise

- use TDD to write tests to satisfy each requirement
- move through the requirements one by one, refactoring your method each time if need be
- think about testing for multiple cases/edge cases

1. fizzBuzz
- Write a “fizzBuzz” method that accepts a number as input and returns it as a String.
- For multiples of three return “Fizz” instead of the number
- For the multiples of five return “Buzz”
- For numbers that are multiples of both three and five return “FizzBuzz”.

2. findMax
- Write a method that takes in an int array, and returns the max value in an array. This can be included 
- Your method should be able to accommodate an array that contains only negative numbers
- Challenge Q: if the user attempts to use the FindMax method with a null input or an empty array, a custom exception should be thrown
